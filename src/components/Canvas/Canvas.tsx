import * as React from 'react';

export default abstract class Canvas extends React.Component<ICanvasProps> {

  protected canvasRef: HTMLCanvasElement | null;
  protected animationFrameRequest: number;
  protected lastDrawTime: number;

  constructor(props: any) {
    super(props);

    this.lastDrawTime = -1;
  }

  componentDidMount() {
    if ( this.props.continuous ) {
      this.animationFrameRequest = requestAnimationFrame(this.draw.bind(this));
    }
  }

  componentDidUpdate() {
    this.draw();
  }

  componentWillUnmount() {
    if ( this.props.continuous )
      cancelAnimationFrame(this.animationFrameRequest);
  }

  abstract setup(): void;
  abstract drawInternal(deltaTime: number, width: number, height: number): void;

  draw() {
    this.setup();

    const currentTime = new Date().getTime();

    this.drawInternal(this.lastDrawTime === -1 ? -1 : (currentTime - this.lastDrawTime!) / 1000.0, this.props.virtualWidth, this.props.virtualWidth * this.props.aspectRatio);

    this.lastDrawTime = currentTime;

    if ( this.props.continuous )
      this.animationFrameRequest = requestAnimationFrame(this.draw.bind(this));
  }

  render() {
    const { pixelWidth, virtualWidth, aspectRatio } = this.props;
    const pixelHeight = pixelWidth * aspectRatio;
    const virtualHeight = virtualWidth * aspectRatio;

    const canvasDimensions = {
      width: pixelWidth,
      height: pixelHeight
    };

    return (
      <canvas
        className="canvas"
        style={canvasDimensions}
        width={virtualWidth}
        height={virtualHeight}
        ref={canvas => this.canvasRef = canvas}
      />
    );
  }
}
