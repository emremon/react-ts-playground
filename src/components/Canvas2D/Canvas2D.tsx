import Canvas from '../Canvas/Canvas';

export default class Canvas2D extends Canvas {
  protected context2D: CanvasRenderingContext2D | null;

  setup(): void {
    if ( this.context2D )
      return;

    if ( !this.canvasRef ) {
      console.log('Could not find a reference to active canvas');
      return;
    }

    if ( this.canvasRef )
      this.context2D = this.canvasRef.getContext('2d');

    return;

  }

  drawInternal(deltaTime: number, width: number, height: number): void {

    if ( this.context2D ) {
      const renderer: ICanvas2DRenderer = this.props.renderer as ICanvas2DRenderer;

      renderer(this.context2D, deltaTime, width, height);
    }
  }

  draw() {

    super.draw();
  }

  render() {

    return super.render();
  }
}
