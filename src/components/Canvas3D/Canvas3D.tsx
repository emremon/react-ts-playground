import Canvas from '../Canvas/Canvas';

export default class Canvas3D extends Canvas {
  protected context3D: WebGLRenderingContext | null;

  setup(): void {
    if ( this.context3D )
      return;

    if (!this.canvasRef) {
      console.log('Could not find a reference to active canvas');
      return;
    }

    const w: any = window;
    if (!w.WebGLRenderingContext) {
      console.log('Browser does not support WebGL');
      return;
    }

    this.canvasRef.addEventListener('webglcontextlost', function(event: Event) { event.preventDefault(); }, false);
    this.canvasRef.addEventListener('webglcontextrestored', this.handleContextLost, false);
    this.context3D = this.canvasRef!.getContext('webgl');

    return;
  }

  handleContextLost(event: Event) {
    event.preventDefault();
    cancelAnimationFrame(this.animationFrameRequest);
    console.log('Not dealing with context lost yet ' + event);
  }

  drawInternal(deltaTime: number, width: number, height: number): void {

    if ( this.context3D ) {
      const renderer: ICanvas3DRenderer = this.props.renderer as ICanvas3DRenderer;

      renderer(this.context3D, deltaTime, width, height);
    }
  }

  draw() {

    super.draw();
  }

  render() {

    return super.render();
  }
}
