import * as React from 'react';
import Canvas2D from '../../components/Canvas2D/Canvas2D';
import Canvas3D from '../../components/Canvas3D/Canvas3D';
import CompositeCanvas from '../../containers/CompositeCanvas/CompositeCanvas';

import './app.css';

export default class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { width: '0', height: '0' };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  public render() {
    const renderer: ICanvas2DRenderer = function(context: CanvasRenderingContext2D, deltaTime: number, width: number, height: number) {
      context.fillStyle = '#000000';
      context.fillRect(1, 1, 1080, 540);
      context.strokeStyle = '#FFFF00';
      context.beginPath();
      context.rect(1, 1, deltaTime * 10 * width, height);
      context.stroke();
    };

    const webGLRenderer: ICanvas3DRenderer = function(gl: WebGLRenderingContext, deltaTime: number, width: number, height: number) {
          // Set clear color to black, fully opaque
      gl.clearColor(0.0, 0.0, 0.0, 1.0);
      // Enable depth testing
      gl.enable(gl.DEPTH_TEST);
      // Near things obscure far things
      gl.depthFunc(gl.LEQUAL);
      // Clear the color as well as the depth buffer.
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    };

    return (
      <div className="App">
        <CompositeCanvas>
          <Canvas3D
            aspectRatio={0.5}
            pixelWidth={this.state.width / 2}
            virtualWidth={1080}
            continuous={true}
            renderer={webGLRenderer}
          />
          <Canvas2D
            aspectRatio={0.5}
            pixelWidth={this.state.width / 2}
            virtualWidth={1080}
            continuous={true}
            renderer={renderer}
          />
        </CompositeCanvas>
      </div>
    );
  }
}
