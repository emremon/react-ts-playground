import * as React from 'react';

import './composite-canvas.css';

export default class CompositeCanvas extends React.Component<any, any> {

  renderChildren() {
    return React.Children.map(this.props.children, child => this.modifyChild(child));
  }

  modifyChild(child: any) {
    return <div className="canvas-expand">{child}</div>;
  }

  render() {
    return (
      <div className="composite-canvas">
        {this.renderChildren()}
      </div>
    );
  }
}
