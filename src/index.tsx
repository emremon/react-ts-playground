import * as React from 'react';
import * as DOM from 'react-dom';
import App from './containers/App/App';

DOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
