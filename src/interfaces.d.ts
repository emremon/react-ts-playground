interface ICanvasProps {
    aspectRatio: number; // Aspect ratio to maintain. Height is width * aspectRatio.
    pixelWidth: number; // Width of canvas in actual pixels.
    virtualWidth: number; // Width of canvas in virtual pixels.
    continuous: boolean; // True of canvas should render itself every frame. False if updates are requested manually through invocations of Canvas.draw().
    renderer: ICanvas2DRenderer | ICanvas3DRenderer; // Renderer to use when drawing content.
}

interface ICanvas2DRenderer {
  (
    context: CanvasRenderingContext2D, // Context that can be used to draw elements
    deltaTime?: number, // Amount of seconds that have passed since last invocation, or -1 if that has not happened before
    width?: number, // Virtual width of canvas.
    height?: number, // Virtual height of canvas.
  ): void;
}

interface ICanvas3DRenderer {
  (
    context: WebGLRenderingContext, // Context that can be used to draw elements
    deltaTime: number, // Amount of seconds that have passed since last invocation, or -1 if that has not happened before
    width?: number, // Virtual width of canvas.
    height?: number, // Virtual height of canvas.
  ): void;
}
